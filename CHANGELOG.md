# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

*For each release, use the following sub-sections:*

- *Added (for new features)*
- *Changed (for changes in existing functionality)*
- *Deprecated (for soon-to-be removed features)*
- *Removed (for now removed features)*
- *Fixed (for any bug fixes)*
- *Security (in case of vulnerabilities)*

## 14/11/2023
### Added
* Set piwikSiteId: "772"

### Fixed
* Updated routes for search pages - same page for all entities
* Fixes in meta tags for bots
* Reverted uikit back to 3.16.24 version
* Minor UI updates

## 31/10/2023
* First deployment of the service
