# RDGraph Portal Documentation

## Introduction

<p>The RDGraph portal is built with Angular, a popular open-source web application framework for building dynamic, single-page applications (SPAs). The current version of Angular in this application is version 16.</p>
<p>Angular Server-Side Rendering (SSR), also known as Angular Universal, is used in this application, which allows rendering on the server, sending pre-rendered HTML to the client's browser.</p>

## System Architecture

![](./src/assets/architecture.png)

<p>
<div>Users can access the RDGraph portal via the <a href="https://beta.rdgraph.openaire.eu" target="_blank">https://beta.rdgraph.openaire.eu</a> domain and browse the contents of the EOSC Research Discovery Graph (RDGraph) or search by keywords and persistent identifiers.</div> 
<div>The search results can be sorted by relevance, date or a variety of impact-based indicators, such as citation count, popularity, influence and impact. </div>
<div>Additionally, simple or advanced filters on multiple metadata fields can be applied to further narrow down the search results.</div> 
<div>The searchable entities within the RDGraph portal are research products (publications, research data, research software, other research products), projects, services (including data sources) and organizations.</div>
</p>
<p>The RDGraph Portal portal also serves detailed pages for specific research products, providing an overview of all the metadata available in RDGraph and the relationships among the entities of the graph, such as related research, supplementary research, funding projects, related organizations and data sources. Users can navigate through the different entities of RDGraph by following the relationships.</p>
<p>Metrics, indicators and charts, calculated by OpenAIRE or external tools, are also available in the detailed pages of the RDGraph portal.</p>

## Building

Run **npm install** to build the application. A folder node-modules containing all the dependent libraries will be created.

### Build - CSR

<div>Use the npm run build-dev for a development build.</div>
<div>Use the npm run build-beta for a beta build.</div>
<div>Use the <b>npm run build-prod</b> for a production build.</div>

### Build - SSR

<div>Use the npm run build:ssr-dev for a development build.</div>
<div>Use the npm run build:ssr-beta for a beta build.</div>
<div>Use the <b>npm run build:ssr-prod</b> for a production build.</div>

## Deployment

### Development server

Run **npm start** for a dev server. Navigate to http://localhost:5200/. The app will automatically reload if you change any of the source files.

### Run SSR

**npm run serve:ssr** will run the last server build.

## Installation
<p>
<div>The application can be deployed in a machine with installed Node.js and NPM (Node Package Manager) by simply running the commands of the Building and Deployment sections.</div>
<div>Current versions of the production deployment are version 16.20.2 for Node.js and version 8.19.4 for NPM.</div>
</p>

<p>
<div>The production deployment is managed with PM2 (Process Manager 2), version 5.3.0, which is a popular and widely used process manager for Node.js applications.</div>
<div>PM2 supports zero-downtime application reloads by gracefully switching to the new version without interrupting user requests and runs the application in the background. The start, stop, restart and managing processes of the application are simplified by the following commands:</div>
<ul>
<li><b>pm2 list</b>: view all the currently running applications managed with PM2 and their statuses.</li>
<li><b>pm2 log</b> {{app_name}}: view the logs of the application {{app_name}}.</li>
<li><b>pm2 start/ stop/ restart</b> {{app_name}}: start/ stop/ restart the application {{app_name}}.</li>
</ul>
</p>

<p>The source code of the project can be found in <a href="https://code-repo.d4science.org/MaDgIK/faircore4eosc-explore" target="_blank">this repository</a>.</p>

## Configuration

An Angular application has several configuration files that help define and manage various aspects of the application. These files are important for project setup, development, and deployment.

<ul>
<li><b>package.json</b>: Specifies the dependencies required for the Angular application and contains scripts that define various tasks, such as commands for starting a development server and building the application.The port used when running the application in client side rendering mode is defined here.</li>
<li><b>angular.json</b>: The angular.json file is the primary configuration file for the application. It defines build and project configuration, including build options, assets, and development server settings.</li>
<li><b>tsconfig.json</b>: The tsconfig.json file is the TypeScript configuration file for the Angular application.</li>
<li><b>environment.ts, environment.beta.ts, environment.prod.ts</b>: These TypeScript files are used to define environment-specific variables and configuration settings. environment.ts is for development, while environment.prod.ts is for production.</li>
<li><b>.gitignore</b>: The .gitignore file specifies files and directories that should be ignored by version control systems like Git. It helps prevent unnecessary or sensitive files from being committed to the repository.</li>
<li><b>.gitmodules</b>: This file includes a reference to other libraries which are used as submodues. The source code of this application consists of the local source code files and assets, as well as of three submodules (libraries).</li>
<li><b>server.ts</b>: This is a file used for server-side rendering (SSR) to render the Angular components and templates on the server. The port used when running the application in server side rendering mode is defined here.</li>

The colors and styling of the user interface are customized in the [eosc-custom.less](https://code-repo.d4science.org/MaDgIK/faircore4eosc-explore/src/branch/main/src/assets/eosc-custom.less) file.

## Changelog

[CHANGELOG file](https://code-repo.d4science.org/MaDgIK/faircore4eosc-explore/src/branch/main/CHANGELOG.md)