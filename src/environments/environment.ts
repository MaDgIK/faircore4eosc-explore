// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The eoscInfo of which env maps to which file can be found in `.angular-cli.json`.

import {EnvProperties} from "../app/openaireLibrary/utils/properties/env-properties";
import {common, commonDev} from "../app/openaireLibrary/utils/properties/environments/environment";

let props: EnvProperties = {
  environment: "development",
  adminToolsPortalType: "faircore4eosc",
  adminToolsCommunity: "faircore4eosc",
  dashboard: "faircore4eosc",
  enablePiwikTrack: false,
  useCache: false,
  useLongCache: false,
  showAddThis: true,
  baseLink : "",
  domain:"https://rdgraph.openaire.eu",
  piwikSiteId: "772",

  enableEoscDataTransfer: false,
  useHelpTexts: false,
  searchLinkToService: "/search/service?serviceId=",
  searchLinkToServices: "/search/find/services",
  searchLinkToAdvancedServices: "/search/advanced/services",
  reCaptchaSiteKey: null,
  footerGrantText : "This OpenAIRE gateway is part of a project that has received funding from the European Union's Horizon 2020 research and innovation programme under grant agreements No. 777541 and 101017452",

  naturalLanguageSearchAPI: "https://darelab.athenarc.gr/nl_search/api/fc4e_get_results/",
  availableCommunitiesAPI: 'https://darelab.athenarc.gr/api/faircore/category-based-recommender/available-communities',
  recommendationsForCommunityAPI: 'https://darelab.athenarc.gr/api/faircore/category-based-recommender/recommend',
  recommendationsForOrcidAPI: 'https://darelab.athenarc.gr/api/faircore/user-to-item-recommender/recommend',
  recommendationsForPublicationAPI: 'https://darelab.athenarc.gr/api/faircore/item-to-item-recommender/recommend/',
  feedbackForRecommendationAPI: 'https://darelab.athenarc.gr/api/faircore/category-based-recommender/update/',

  searchAPIURLLAst: "http://services.openaire.eu/search/v2/api/",
  searchResourcesAPIURL: "https://services.openaire.eu/search/v2/api/resources",
  csvAPIURL: "https://services.openaire.eu/search/v2/api/reports",

  indexInfoAPI: "https://services.openaire.eu/openaire/info/",
  pdfStatisticsAPIURL: "https://services.openaire.eu/pdf-stats",
  statisticsFrameNewAPIURL: "https://services.openaire.eu/stats-tool/",
  cacheUrl: "https://explore.openaire.eu/cache/get?url=",

  pidResolverParserAPI:"http://dl170.madgik.di.uoa.gr:3000/get/",
  showRecommendations: true
};

export let properties: EnvProperties = {
  ...common, ...commonDev, ...props
}