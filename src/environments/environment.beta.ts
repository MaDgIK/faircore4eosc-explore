// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The eoscInfo of which env maps to which file can be found in `.angular-cli.json`.

import {EnvProperties} from "../app/openaireLibrary/utils/properties/env-properties";
import {common, commonProd} from "../app/openaireLibrary/utils/properties/environments/environment";

let props: EnvProperties = {
  environment: "beta",
  adminToolsPortalType: "faircore4eosc",
  adminToolsCommunity: "faircore4eosc",
  dashboard: "faircore4eosc",
  enablePiwikTrack: true,
  useCache: false,
  useLongCache: true,
  showAddThis: true,
  baseLink : "",
  domain:"https://beta.rdgraph.openaire.eu",
  piwikSiteId: "772",

  enableEoscDataTransfer: false,
  useHelpTexts: false,
  searchLinkToService: "/search/service?serviceId=",
  searchLinkToServices: "/search/find/services",
  searchLinkToAdvancedServices: "/search/advanced/services",
  reCaptchaSiteKey: null,
  footerGrantText : "",

  searchAPIURLLAst: "https://services.openaire.eu/search/v2/api/",
  searchResourcesAPIURL: "https://services.openaire.eu/search/v2/api/resources",
  csvAPIURL: "https://services.openaire.eu/search/v2/api/reports",
  naturalLanguageSearchAPI: "https://darelab.athenarc.gr/nl_search/api/fc4e_get_results/",
  availableCommunitiesAPI: 'https://darelab.athenarc.gr/api/faircore/category-based-recommender/available-communities',
  recommendationsForCommunityAPI: 'https://darelab.athenarc.gr/api/faircore/category-based-recommender/recommend/',
  recommendationsForOrcidAPI: 'https://darelab.athenarc.gr/api/faircore/user-to-item-recommender/recommend',
  recommendationsForPublicationAPI: 'https://darelab.athenarc.gr/api/faircore/item-to-item-recommender/recommend/',
  feedbackForRecommendationAPI: 'https://darelab.athenarc.gr/api/faircore/category-based-recommender/update/',
  showRecommendations: true,
  pidResolverParserAPI: "https://beta18.explore.openaire.eu/pid-parser-service/"
};

export let properties: EnvProperties = {
  ...common, ...commonProd, ...props
}