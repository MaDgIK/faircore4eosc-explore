import { NgModule } from '@angular/core';

import { SharedModule } from '../../openaireLibrary/shared/shared.module';
import { OpenaireClaimsAdminComponent } from './claimsAdmin.component';
import { ClaimsAdminRoutingModule } from './claimsAdmin-routing.module';
import{ClaimsAdminModule } from '../../openaireLibrary/claims/claimsAdmin/claimsAdmin.module';
import{  ClaimsCuratorGuard} from '../../openaireLibrary/login/claimsCuratorGuard.guard';

@NgModule({
  imports: [
    SharedModule,
    ClaimsAdminRoutingModule,
    ClaimsAdminModule
  ],
    providers:[ClaimsCuratorGuard],
  declarations: [
    OpenaireClaimsAdminComponent
  ]
})
export class LibClaimsAdminModule { }
