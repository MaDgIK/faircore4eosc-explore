import {Component} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Subscriber} from "rxjs";
import {properties} from "../../../environments/environment";



@Component({
    selector: 'openaire-my-claims',
    template: `
    <my-claims *ngIf="claimsInfoURL" [claimsInfoURL]=claimsInfoURL>
</my-claims>
`

})
 export class OpenaireMyClaimsComponent {
  claimsInfoURL:string;
  sub;

  constructor (private route: ActivatedRoute) {
  }
  ngOnDestroy() {
    if (this.sub instanceof Subscriber) {
      this.sub.unsubscribe();
    }
  }
   public ngOnInit() {
     this.sub = this.route.data
         .subscribe(() => {
            this.claimsInfoURL = properties.claimsInformationLink;
         });
   }
}
