import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {CategoryToItemComponent} from './category-to-item/category-to-item.component';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'category-to-item', component: CategoryToItemComponent, 
      },
      {
        path: 'category-to-item/:community', component: CategoryToItemComponent
      }
    ])
  ]
})
export class RecommendationsRoutingModule { }
