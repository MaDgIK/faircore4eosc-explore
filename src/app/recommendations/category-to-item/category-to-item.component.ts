import {ChangeDetectorRef, Component} from "@angular/core";
import {CommunitiesService} from "../../openaireLibrary/connect/communities/communities.service";
import {EnvProperties} from "../../openaireLibrary/utils/properties/env-properties";
import {properties} from "../../../environments/environment";
import {CommunityInfo} from "../../openaireLibrary/connect/community/communityInfo";
import {Subscriber, zip} from "rxjs";
import {FormBuilder, FormControl} from "@angular/forms";
import {debounceTime, distinctUntilChanged} from "rxjs/operators";
import {RecommendationsService} from "../../openaireLibrary/recommendations/recommendations.service";
import {ActivatedRoute, Router} from "@angular/router";
import {UserManagementService} from "../../openaireLibrary/services/user-management.service";
import {User} from "../../openaireLibrary/login/utils/helper.class";
import {LayoutService} from "../../openaireLibrary/dashboard/sharedComponents/sidebar/layout.service";
import {HelperFunctions} from "../../openaireLibrary/utils/HelperFunctions.class";

@Component({
  selector: 'category-to-item',
  templateUrl: './category-to-item.component.html',
  styleUrls: ['category-to-item.component.less']
})

export class CategoryToItemComponent {
  private subscriptions = [];
  properties: EnvProperties = properties;

  showLoading: boolean = true;
  errorMessage: string;
  offset: number;

  activeCommunity: string;
  size: number = 5; // show search-bar only when communities are more than this number - defaults to 5
  communities: CommunityInfo[];
  filteredCommunities: CommunityInfo[];
  recommendations: any;
  user: User;

  keywordControl: FormControl;
  keyword: string;

  isMobile: boolean;

  constructor(private communitiesService: CommunitiesService,
              private recommendationsService: RecommendationsService,
              private router: Router,
              private route: ActivatedRoute,
              private fb: FormBuilder,
              private cdr: ChangeDetectorRef,
              private userManagementService: UserManagementService,
              private layoutService: LayoutService) {
  }

  ngOnInit() {
    this.properties = properties;
    if (typeof document !== "undefined") {
      this.offset = Number.parseInt(getComputedStyle(document.documentElement).getPropertyValue('--header-height'));
    }
    this.layoutService.isMobile.subscribe(isMobile => {
			this.isMobile = isMobile;
			this.cdr.detectChanges();
		});
    this.getCommunities();
    this.subscriptions.push(this.userManagementService.getUserInfo().subscribe(user => {
      this.user = user;
    }));
    this.subscriptions.push(this.route.params.subscribe(params => {
      if(params && params['community']) {
        this.subscriptions.push(this.recommendationsService.getRecommendationsForCommunity(this.properties.recommendationsForCommunityAPI, params['community'], this.user ? this.user.id : null).subscribe(data => {
          this.recommendations = data;
          this.setActiveCommunity(params['community']);
        }, error => {
          if(error.status == 422) {
            this.errorMessage = 'We could not find any recommendations for this community.'
          } else {
            this.errorMessage = 'Sorry, something went wrong.';
          }
        }));
      }
    }));
    this.keywordControl = this.fb.control('');
    this.subscriptions.push(this.keywordControl.valueChanges.pipe(debounceTime(300), distinctUntilChanged()).subscribe(value => {
      this.keyword = value;
      this.filtering();
    }));
  }

  ngOnDestroy() {
		this.subscriptions.forEach(subscription => {
			if (subscription instanceof Subscriber) {
        subscription.unsubscribe();
			}
		});
	}

  private getCommunities() {
    this.subscriptions.push(zip(this.recommendationsService.getAvailableCommunities(this.properties.availableCommunitiesAPI), this.communitiesService.getCommunities(this.properties, this.properties.communitiesAPI)).subscribe(data => {
      let availableCommunities: any = data[0];
      this.communities = availableCommunities.map(id => {
        let obj = data[1].find(o => o.communityId === id);
        if(!obj) {
          // return null;
          obj = new CommunityInfo();
          obj.communityId = id;
          obj.displayTitle = id;
          obj.displayShortTitle = id;
        }
        return { id, ...obj };
      });
      this.communities = this.communities.filter(com => com != null);
      this.communities.sort((a, b) => a['displayTitle'].localeCompare(b['displayTitle']));
      this.filteredCommunities = this.communities;
      this.showLoading = false;
    }));
  }

  filtering() {
    let filteredCommunities = this.communities;
    if(!this.keyword){
      this.keyword = '';
    }
    if(this.communities.length) {
			filteredCommunities = filteredCommunities.filter(item => (item['displayTitle'] && item['displayTitle'].toLowerCase().includes(this.keyword.toLowerCase())) || (item['displayShortTitle'] && item['displayShortTitle'].toLowerCase().includes(this.keyword.toLowerCase())));
		}
    this.filteredCommunities = filteredCommunities;
  }

  selectCommunity(id: string) {
    this.errorMessage = null;
    this.router.navigate(['/recommendations/category-to-item/' + id]);
  }

  setActiveCommunity(id) {
    this.activeCommunity = id;
  }

  scrollToId(value: string) {
    HelperFunctions.scrollToId(value);
  }
}