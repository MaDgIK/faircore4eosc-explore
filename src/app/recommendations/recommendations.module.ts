import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {RouterModule} from "@angular/router";
import {CategoryToItemComponent} from "./category-to-item/category-to-item.component";
import {RecommendationsRoutingModule} from "./recommendations-routing.module";
import {SearchInputModule} from "../openaireLibrary/sharedComponents/search-input/search-input.module";
import {CommunitiesService} from "../openaireLibrary/connect/communities/communities.service";
import {IconsModule} from "../openaireLibrary/utils/icons/icons.module";
import {LoadingModule} from "../openaireLibrary/utils/loading/loading.module";
import {RecommendationsService} from "../openaireLibrary/recommendations/recommendations.service";
import {RecommendationCardModule} from "../openaireLibrary/recommendations/recommendation-card.module";

@NgModule({
  imports: [
    RecommendationsRoutingModule, CommonModule, RouterModule, IconsModule,
    SearchInputModule, LoadingModule, RecommendationCardModule
  ],
  declarations: [
    CategoryToItemComponent
  ],
  providers: [
    CommunitiesService, RecommendationsService
  ],
  exports: [
    CategoryToItemComponent
  ]
})

export class RecommendationsModule {
  constructor() {

  }
}