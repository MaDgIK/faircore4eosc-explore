import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {RouterModule} from "@angular/router";
import {SearchInputModule} from "../openaireLibrary/sharedComponents/search-input/search-input.module";
import {IconsModule} from "../openaireLibrary/utils/icons/icons.module";
import {LoadingModule} from "../openaireLibrary/utils/loading/loading.module";
import {NaturalLanguageSearchModuleRoutingModule} from "./natural-language-search-routing.module";
import {NaturalLanguageSearchComponent} from "./natural-language-search.component";
import {NaturalLanguageSearchService} from "./natural-language-search.service";

@NgModule({
  imports: [
    NaturalLanguageSearchModuleRoutingModule, CommonModule, RouterModule, IconsModule,
    SearchInputModule, LoadingModule
  ],
  declarations: [
    NaturalLanguageSearchComponent
  ],
  providers: [
    NaturalLanguageSearchService
  ],
  exports: [
    NaturalLanguageSearchComponent
  ]
})

export class NaturalLanguageSearchModule {
  constructor() {
  }
}