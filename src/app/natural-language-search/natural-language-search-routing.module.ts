import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {NaturalLanguageSearchComponent} from './natural-language-search.component';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: '', component: NaturalLanguageSearchComponent, 
      },
    ])
  ]
})
export class NaturalLanguageSearchModuleRoutingModule { }
