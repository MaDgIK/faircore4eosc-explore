import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Injectable} from "@angular/core";

@Injectable()

export class NaturalLanguageSearchService {
  constructor(private http: HttpClient) {
  }

  getResults(url: string, queryPhrase: string) {
    const body = {
      "nl_query": queryPhrase,
    };
    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    return this.http.post(url, body, options);
  }
}