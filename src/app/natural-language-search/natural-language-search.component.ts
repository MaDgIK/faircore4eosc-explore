import {Component} from "@angular/core";
import {Subscriber} from "rxjs";
import {NaturalLanguageSearchService} from "./natural-language-search.service";
import {EnvProperties} from "../openaireLibrary/utils/properties/env-properties";
import {properties} from "../../environments/environment";

@Component({
  selector: 'natural-language-search',
  templateUrl: './natural-language-search.component.html',
  styleUrls: ['natural-language-search.component.less']
})

export class NaturalLanguageSearchComponent {
  // private subscriptions = [];
  private nlSearchSubscription = null;

  properties: EnvProperties = properties;
  showLoading: boolean = false;
  errorMessage: string;

  phrase: string;
  queryPhrase: string;
  results;
  indexColumnId: number;

  constructor(private naturalLanguageSearchService: NaturalLanguageSearchService) {
  }

  ngOnDestroy() {
		// this.subscriptions.forEach(subscription => {
		// 	if (subscription instanceof Subscriber) {
    //     subscription.unsubscribe();
		// 	}
		// });
    if (this.nlSearchSubscription instanceof Subscriber) {
      this.nlSearchSubscription.unsubscribe();
    }
	}

  search() {
    this.results = null;
    this.queryPhrase = this.phrase;
    this.errorMessage = null;
    this.showLoading = true;
    if(this.nlSearchSubscription) {
      this.nlSearchSubscription.unsubscribe();
    }
    this.nlSearchSubscription = this.naturalLanguageSearchService.getResults(this.properties.naturalLanguageSearchAPI, this.queryPhrase).subscribe(data => {
    // this.subscriptions.push(this.naturalLanguageSearchService.getResults(this.properties.naturalLanguageSearchAPI, this.queryPhrase).subscribe(data => {
      if(data && data['data'] && data['data'].length > 0) {
        if(data && data['columns']) {
          for(let i = 0; i < data['columns'].length - 1; i++) {
            if(data['columns'][i] == 'id') {
              this.indexColumnId = i;
              break;
            }
          }
        }
        this.results = data;
      } else {
        this.results = [];
      }

      this.showLoading = false;
    }, error => {
      if(error.status == 500) {
        this.errorMessage = error.error.message;
      } else {
        this.errorMessage = 'Sorry, something went wrong. Please try again later.';
      }
      this.showLoading = false;
    });
  }
}